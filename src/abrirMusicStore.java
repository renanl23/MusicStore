public class abrirMusicStore {
    public static void main(String[] args) {

        Artista hayley = new Artista("Hayley Williams");
        Artista taylor = new Artista("Taylor York");
        Artista zac = new Artista("Zac Farro");

        Artista[] integrantes = { hayley, taylor, zac };

        Banda paramore = new Banda("Paramore", "rock", 2004, integrantes);

        Musica decode = new Musica("Decode", 4.40, paramore, "https://www.youtube.com/watch?v=RvnkAtWcKYg");

        System.out.println(String.format("%s - %s. Disponível em:\n%s", decode.getNome(), decode.getBanda().getNome(),
                decode.getYoutubeLink()));
    }
}
