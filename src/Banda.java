public class Banda {
  private String nome;
  private String genero;
  private Integer anoFundado;
  private Artista[] artistas;

  public Banda(String nome, String genero, Integer anoFundado, Artista[] artistas) {
    this.nome = nome;
    this.genero = genero;
    this.anoFundado = anoFundado;
    this.artistas = artistas;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Integer getAnoFundado() {
    return anoFundado;
  }

  public void setAnoFundado(Integer anoFundado) {
    this.anoFundado = anoFundado;
  }

  public String getGenero() {
    return genero;
  }

  public void setGenero(String genero) {
    this.genero = genero;
  }

  public Artista[] getArtistas() {
    return artistas;
  }

  public void setArtistas(Artista[] artistas) {
    this.artistas = artistas;
  }
}
