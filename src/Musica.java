public class Musica {
  private String nome;
  private Double duracaoMinutos;
  private Banda banda;
  private String youtubeLink;

  public Musica(String nome, Double duracaoMinutos, Banda banda, String youtubeLink) {
    this.nome = nome;
    this.duracaoMinutos = duracaoMinutos;
    this.banda = banda;
    this.youtubeLink = youtubeLink;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Double getDuracaoMinutos() {
    return duracaoMinutos;
  }

  public void setDuracaoMinutos(Double duracaoMinutos) {
    this.duracaoMinutos = duracaoMinutos;
  }

  public Banda getBanda() {
    return banda;
  }

  public void setBanda(Banda banda) {
    this.banda = banda;
  }

  public String getYoutubeLink() {
    return youtubeLink;
  }

  public void setYoutubeLink(String youtubeLink) {
    this.youtubeLink = youtubeLink;
  }
}